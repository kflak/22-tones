(
var scale = Scale.majorPentatonic;
var ms;
var mset = MarkovSet([
    [0, [1, 2, 7]],
    [1, [2, 3]],
    [2, [3, 4]],
    [3, [4, 6]],
    [4, [5, 6]],
    [5, [6, 7]],
    [6, [0, 7]],
    [7, [0, 6]],
]);
var motifs = [
    [0, 1, 2, 3, 5],
    [1, 2, 6],
    [3, 4, 0],
    [6, 3, 0],
    [2, 4, 6, 1],
    [2, 3, 4],
    [4, 5, 4, 5],
    [6, 7, 6, 9],
];
mset.makeSeeds;
ms = mset.asStream;

~mbMelodicFx = FxChain.new(
    out: ~masterBus,
    level: -6.dbamp,
    fadeInTime: 10,
    fadeOutTime: 30,
);
~mbMelodicFx.add(\comb, [
    \delay, Pwhite(80, 90).midicps.reciprocal, Pwhite(2, 5),
    \decay, 2, 
    \mix, Pwhite(0.01, 0.2),
    \lag, 2,
]);
~mbMelodicFx.add(\ringMod, [
    \modfreq, Pwhite(3.0, 10.0), Pwhite(2, 4),
    \depth, 1,
    \lag, 3,
    \mix, 0.3, 
]);
~mbMelodicFx.add(\ringMod, [
    \modfreq, Pwhite(1.0, 8.0), Pwhite(2, 3),
    \depth, 1,
    \lag, 2,
    \mix, 0.3, 
]);
~mbMelodicFx.add(\noiseMod, [
    \modfreq, Pwhite(10.0, 40.0), Pwhite(2, 4),
    \depth, 2,
    \mix, Pwhite(0.3, 1.0), Pwhite(2, 4),
    \lag, 3,
]);
~mbMelodicFx.add(\eq, [
    \locut, 80,
    \hishelf, 600,
    \hishelfdb, -9,
]);
~mbMelodicFx.addPar( 
    \comb, [\mix, 0.3, \delay, 0.6, \decay, 1, \amp, 1/3],
    \comb, [\mix, 0.3, \delay, 1.5, \decay, 3, \amp, 1/3],
    \comb, [\mix, 0.3, \delay, 0.7, \decay, 1, \amp, 1/3],
);
~mbMelodicFx.add(\comb, [
    \delay, 0.125,
    \decay, 2, 
    \mix, 0.3,
]);
~mbMelodicFx.add(\jpverb, [
    \revtime, 3,
    \mix, 0.2,
]);

~mbMelodic = ~mb.collect{|id, idx|
    MBDeltaTrig.new( 
        speedlim: 0.5, 
        threshold: 0.05, 
        minibeeID: id,
        // minAmp: -40,
        // maxAmp: -12,
        function: {|dt| 
            var deg, dur, totalDur, octave;
            var phraseIndex = ms.next;
            deg = motifs[phraseIndex];
            dur = rrand(0.2, 2);
            octave = rrand(5, 7);
            totalDur = (dur * deg.size * rrand(0.3, 2.0));
            Pbind(
                \instrument, \pm,
                \octave, Pwrand((6..8), [1, 2, 2].normalizeSum, inf),
                \pmindex, Pwhite(0.2, 0.5),
                \modfreq, Pfunc {|ev| ev.use { ~freq * rrand(0.98, 1.02)}},
                \degree, Pseq(deg),
                \amp, Pfunc {|ev| ev.use {8 / ~freq}} * dt.linlin(0.0, 1.0, 0.1, 0.3),
                \release, Pkey(\dur) * 2,
                \dur, Pwhite(dur/2, dur),
                \attack, Pkey(\dur) / Pwhite(1, 4),
                \legato, 0.9,
                \out, ~mbMelodicFx.in,
                \group, ~mbMelodicFx.group,
            ).play;
        }
    )
}
)

// (
// ~mbMelodicFx.play;
// ~mbMelodic.do(_.play);
// )

// (
// ~mbMelodicFx.free;
// ~mbMelodic.do(_.free); 
// )
