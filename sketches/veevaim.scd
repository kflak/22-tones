( 
    var currentPos = 0;
    var initTime = thisThread.seconds;

    ~veevaimFx = List.new(0);
    4.do{
        ~veevaimFx.add(
            FxChain.new(
                fadeInTime: 30,
                level: 12.dbamp,
                fadeOutTime: 30,
                out: ~masterBus,
            );
        );
    };
    ~veevaimFx[0].addPar(
        \comb, [\mix, 0.3, \delay, 0.2, \decay, 1, \amp, 1/3],
        \comb, [\mix, 0.3, \delay, 0.5, \decay, 1, \amp, 1/3],
        \comb, [\mix, 0.3, \delay, 0.7, \decay, 1, \amp, 1/3],
    ); 
    ~veevaimFx[0].add(\eq, [
        // \locut, 120
        \locut, 440,
        \hicut, 600,
    ]); 
    //--------------------------------------------------------- 
    ~veevaimFx[1].add(\jpverb, [
        \revtime, 3,
        \mix, 0.3
    ]); 
    ~veevaimFx[1].add(\eq, [
        // \locut, 120
        \locut, 440,
        \hicut, 600,
    ]); 
    //--------------------------------------------------------- 
    ~veevaimFx[2].add(\greyhole, [
        \delayTime, 0.3,
        \feedback, 0.9,
        \mix, 0.3
    ]); 
    ~veevaimFx[2].add(\eq, [
        // \locut, 120
        \locut, 440,
        \hicut, 600,
    ]); 
    //--------------------------------------------------------- 
    ~veevaimFx[3].add(\eq, [
        // \locut, 120
        \locut, 440,
        \hicut, 600,
    ]); 

    ~veevaim = ~mb.collect{|id, idx|
        MBDeltaTrig.new(
            speedlim: 0.5, 
            threshold: 0.02,
            minibeeID: id,
            minAmp: -16,
            maxAmp: -6,
            function: {|dt, minAmp, maxAmp|
                var currentFx;
                var buf = ~buf[\veevaim];
                var numFrames = buf[0].numFrames;
                var dur = 0.2;
                var step = dur * s.sampleRate;
                var len = dt.linlin(0.0, 1.0, step, step * 10);
                var pos = (currentPos, currentPos+step..currentPos+len); 
                var currentTime = thisThread.seconds - initTime;
                currentPos = pos[pos.size-1]; 
                // currentPos = pos[pos.size-1].mod(numFrames); 


                if(currentPos<numFrames){
                    Pbind(
                        \instrument, \playbuf,
                        \buf, Prand(buf),
                        \dur, dur,
                        \attack, Pkey(\dur),
                        \release, Pkey(\dur) * 4,
                        \startPos, Pseq(pos),
                        \legato, 2,
                        \rate, Pwhite(0.99, 1.01),
                        \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                        \pan, Pwhite(-1.0, 1.0),
                        \out, Pfunc({
                            currentFx = ~veevaimFx.choose;
                            currentFx.in
                        }),
                        \group, Pfunc({ 
                            currentFx.group
                        }),
                    ).play;
                }{ 
                    ~veevaimFx.do(_.free);
                    ~veevaim.do(_.free);
                }
            }
        );
    }; 
)

~veevaimFx.do(_.play); ~veevaim.do(_.play);
~veevaimFx.do(_.free); ~veevaim.do(_.free);
