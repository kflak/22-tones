(
var scale = ~superpyth;
// var scale = Scale.new([0, 4, 5, 9, 13, 17, 18], 22, tuning: \et22, name: "superpyth 7");
var partials = [1.0, 2.01, 4.02, 8.01];
var nPartials = partials.size;
var rootFreq = 442;

~additiveMBTestFx = FxChain.new(
    level: 3.dbamp,
    out: ~masterBus,
    fadeInTime: 1,
    fadeOutTime: 1,
);
// ).play;

// s.sync;

~additiveMBTestFx.add(\flanger,[
    \depth, 0.2,
    \mix, 0.8,
]);

~additiveMBTestFx.add(\ringMod,[
    \depth, 2,
    \modfreq, Pwhite(5, 10),
    \mix, 0.2,
]);

// ~additiveMBTestFx.add(\granulator,[
//     \decay, 2,
//     \grainsize, 0.05,
//     \freq, Pwhite(10, 15),
//     \rate, 2,
//     \mix, 0.5,
// ]);

~additiveMBTestFx.add(\jpverb,[
    \revtime, 3,
    \mix, Pwhite(0.2, 0.3),
]);

~additiveMBTestFx.add(\eq,[
    \hishelfdb, -6,
    \hishelffreq, 800,
    \locut, 120,
    \mix, 1.0,
]);

~additiveMBTest = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.03, 
        minibeeID: id,
        minAmp: -16,
        maxAmp: -9,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \additive,
                \note, Prand((0..12), dt.linlin(0.0, 1.0, 1, 4)),
                \freq, Pfunc{|ev| Ref( scale.degreeToFreq(ev.note, rootFreq, rrand(-1, 1)) ) },
                \freqs, Pfunc{|ev| Ref( partials.collect{ |i| ev.freq * i * rrand(0.99, 1.01) }) },
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \amps, Pfunc{ Ref( Array.interpolation(nPartials, 1, 0.1).pow(3) )},
                \dur, dt.linlin(0.0, 1.0, 0.2, 0.02),
                \attack, 2,
                \release, dt.linlin(0.0, 1.0, 1, 5) * [0.2, 1].wchoose([0.3, 0.7]),
                \out, ~additiveMBTestFx.in,
                \group, ~additiveMBTestFx.group,
            ).play;
        }
    );
    // ).play;
}
) 

// ~additiveMBTestFx.play; ~additiveMBTest.do(_.play); 
// ~additiveMBTest.do(_.free); ~additiveMBTestFx.free;
