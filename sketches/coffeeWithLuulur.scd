(
var revMB = 14;
var rateMB = 10;
fork{
    5.wait;
    ~coffeeWithLuulurFx = FxChain.new(
        level: 0.dbamp,
        out: ~masterBus,
        fadeInTime: 1,
        fadeOutTime: 1,
    ).play;

    s.sync;

    ~coffeeWithLuulurFx.add(\jpverb,[
        \revtime, 3,
        \mix, Pfunc{~mbData[9].xbus.getSynchronous.pow(2) }, 0.1,
    ]);

    ~coffeeWithLuulur = Synth(\playbuf, [
        \buf, ~buf[\luulur1][0],
        \out, ~coffeeWithLuulurFx.in,
        \loop, 0,
    ], target: ~coffeeWithLuulurFx.group);

    Tdef(\coffeeWithLuulur, {
        var rate;
        inf.do{
            rate = ~mbData[10].xbus.getSynchronous;
            rate = rate.linexp(0.0, 1.0, 4, 0.5);
            ~coffeeWithLuulur.set(\rate, rate);
            0.05.wait;
        };
    }).play;    
}
)

// ~coffeeWithLuulur.release(1); ~coffeeWithLuulurFx.free; Tdef(\coffeeWithLuulur).stop;
