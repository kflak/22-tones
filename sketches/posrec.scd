(
var scale = ~superpyth;
var stopPose = 8;
var isPlaying = false;
var triggerMB = 9;
var synth;

~mbData[9].sendOscOut = true;
~mbData[10].sendOscOut = true;

~posrecFx = FxChain.new(
    level: 0.dbamp,
    out: ~masterBus,
    fadeInTime: 1,
    fadeOutTime: 1,
);


~posrecFx.addPar(
    \comb, [\mix, 0.3, \delay, 0.2, \decay, 1, \amp, 1/3],
    \comb, [\mix, 0.3, \delay, 0.5, \decay, 1, \amp, 1/3],
    \comb, [\mix, 0.3, \delay, 0.7, \decay, 1, \amp, 1/3],
);

~posrecFx.add(\jpverb,[
    \revtime, 3,
    \mix, 0.7,
]);

~posrec = MBDeltaTrig.new(
    speedlim: 3,
    threshold: 0.05, 
    minibeeID: triggerMB,
    minAmp: -20,
    maxAmp: 0,
    function: {|dt, minAmp, maxAmp|
        var octave =  2;
        var deg = (0..6).choose;
        var freq = scale.degreeToFreq(deg, 442, octave);
        var freqs = Ref(Array.geom(8, freq, rrand(1.2, 4) ));
        var length = dt.linlin(0.0, 1.0, 2, 6);

        if(isPlaying.not){
            synth = Synth(\additive, [
                \attack, 5,
                \release, 2,
                \freqs, freqs,
                \out, ~posrecFx.in,
            ], target: ~posrecFx.group);

            isPlaying = true;

            ~mbData[9].sendOscOut = true;
            ~mbData[10].sendOscOut = true;

            OSCdef(\posrec, {|msg| 
                var pose = msg[1];
                if(pose < stopPose){
                    Pbind(
                        \type, \set,
                        \id, synth.nodeID,
                        \args, #[\freqs, \amps, \lag, \pan, \amp],
                        \dur, Pwrand([0.25, 0.333, 0.5], [2, 2, 1,].normalizeSum, length),
                        \scale, scale,
                        \degree, pose,
                        // \degree, Pseq(Array.series(length, deg+1, 1), 1),
                        \octave, octave,
                        \freqs, Pfunc{|ev| 
                            var f = ev.use {~freq.() };
                            // Ref(Array.geom(8, f, 2))
                            Ref(Array.geom(8, f, rrand(1.99, 2.01)))
                        },
                        \amps, Pfunc{ Ref(Array.geom(8, 0.7, 0.6) )},
                        \lag, Pwhite(0.3, 0.8),
                        \pan, Pwhite(-1.0, 1.0), 
                        \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                    ).play;
                }{
                    ~freePosrec.();
                    isPlaying = false;
                };
                pose.postln; 
            }, "/mbposrec/index");
        } 
    }
);


~freePosrec = {
    OSCdef(\posrec).free;
    ~mbData[9].sendOscOut = false;
    ~mbData[10].sendOscOut = false;
    synth.release(1);
};
) 

// ~posrecFx.play; ~posrec.play;
// ~posrecFx.free; ~posrec.free; ~freePosrec.value();
