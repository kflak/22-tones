# 22 Tonal Workshop

This is a SuperCollider repo for the Pärnu New Music days 2021 microtonality workshop. It contains the necessary files to run MiniBee sensor patches, but not the audio files for the required buffers. Please get in touch if you want these files. The repo also contains a standalone synthesizer to be able to play a 22-tone chromatic scale on a standard MIDI keyboard.

## Requirements

In addition to MiniBees and their associated software the repo depends on:
- [MiniBeeUtils](https://gitlab.com/kflak/minibeeutils)
- The soundFile extension in [this](https://gitlab.com/kflak/plus) repo
- [KFUtils](https://gitlab.com/kflak/kfutils)

In order to install them on your system, open up SuperCollider and run this code:

```
(
Quarks.install("https://gitlab.com/kflak/minibeeutils");
Quarks.install("https://gitlab.com/kflak/plus");
Quarks.install("https://gitlab.com/kflak/kfutils");
)
```
Recompile or restart SuperCollider.

## Getting Started

After installing the necessary quarks, evaluate the file called `init.scd` to set everything up. The `sketches` folder contains several sketches for MiniBee sensors. Evaluate the body of the file, then uncomment the lines at the bottom of the page and run the `play` functions. To stop them, run the `free` functions below that.
