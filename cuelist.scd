(
~cuelist = CuePlayer.new;

~cuelist.add(
    timeline: [
        0, {
            "025 Not Quite Ready".postln;
            this.executeFile(~sketchdir+/+"em.scd");
            1.wait;
            ~emFx.play;
            ~em.do(_.play);
        }
    ],
    function: {
        ~cuelist.hook = {|cl| 
            ~emFx.free;
            ~em.do(_.free);
        };
    }
);

~cuelist.add(
    timeline: [
        0, {
            "028 Rooftop".postln;
            this.executeFile(~sketchdir+/+"stormulator.scd");
            1.wait;
            ~stormulatorFx.play;
            ~stormulator[0..3].do(_.play);
        }
    ],
    function: {
        ~cuelist.hook = {|cl| 
            ~stormulatorFx.free;
            ~stormulator[0..3].do(_.free);
        };
    }
);

~cuelist.add(
    timeline: [
        0, {
            "002 Granular Trumpet".postln;
            this.executeFile(~sketchdir+/+"trumpetulator.scd");
            1.wait;
            ~trumpetulatorFx.play;
            ~trumpetulator[4..7].do(_.play);
        }
    ],
    function: {
        ~cuelist.hook = {|cl| 
            ~trumpetulatorFx.free;
            ~trumpetulator[4..7].do(_.free);
        };
    }
);

~cuelist.add(
    timeline: [
        0, {
            "024 Needle Cocoon".postln;
            this.executeFile(~sketchdir+/+"mbFbnoise.scd");
            1.wait;
            ~mbFbnoiseFx.play;
            ~mbFbnoise.do(_.play);
        }
    ],
    function: {
        ~cuelist.hook = {|cl| 
            ~mbFbnoiseFx.free;
            ~mbFbnoise.do(_.free);
        };
    }
);

~cuelist.add(
    timeline: [
        0, {
            "023 Hands".postln;
            this.executeFile(~sketchdir+/+"mbMelodic.scd");
            1.wait;
            ~mbMelodicFx.play;
            ~mbMelodic.do(_.play);
        }
    ],
    function: {
        ~cuelist.hook = {|cl| 
            ~mbMelodicFx.free;
            ~mbMelodic.do(_.free);
        };
    }
);

if ( ~cuelist.cueList.size > 0 ){
    "cuelist loaded".postln;
}{
        "*** WARNING: Cuelist not loaded".postln;
}

)
