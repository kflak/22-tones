(
~cuelist = CuePlayer.new;

~cuelist.add( {
    fork{
        "em".postln;
        this.executeFile(~sketchdir+/+"em.scd");
        s.sync;
        ~emFx.play;
        ~em[0..3].do(_.play);
    };

    ~cuelist.hook = {|cl| 
        ~emFx.free;
        ~em[0..3].do(_.free);
    };
});

~cuelist.add( { ~cuelist.hook = {|cl| } });

~cuelist.add( {
    var synth, routine;
    "smooth control".postln;
    synth = Synth(\sine, [\lag, 0.2, \amp, 0.3]);
    routine = Routine {
        inf.do{
            synth.set(\freq, ~mbData[9].x.linexp(0.0, 1.0, 221, 442));
            0.05.wait;
        }
    }.play;
    ~cuelist.hook = {|cl| 
        synth.release(2);
        routine.stop;
    };
});

~cuelist.add( { ~cuelist.hook = {|cl| } });

// ~cuelist.add( {
//     // 12-tone chromatic scale
//     var synth, routine;
//     synth = Synth(\sine, [\lag, 0.01, \amp, 0.1]);
//     routine = Routine {
//         var scale = Scale.chromatic;
//         inf.do{
//             var degree = ~mbData[9].x.linlin(0.0, 1.0, 0, 12.9).floor;
//             var freq = scale.degreeToFreq(degree, 442, -1);
//             synth.set(\freq, freq);
//             0.1.wait;
//         }
//     }.play;
//     ~cuelist.hook = {|cl| 
//         synth.release(2);
//         routine.stop;
//     };
// });

// ~cuelist.add();

// ~cuelist.add( {
//     // major scale
//     var synth, routine;
//     synth = Synth(\sine, [\lag, 0.01, \amp, 0.3]);
//     routine = Routine {
//         var scale = Scale.major;
//         inf.do{
//             var degree = ~mbData[9].x.linlin(0.0, 1.0, 0, 7.9).floor;
//             var freq = scale.degreeToFreq(degree, 442, -1);
//             synth.set(\freq, freq);
//             0.1.wait;
//         }
//     }.play;
//     ~cuelist.hook = {|cl| 
//         synth.release(2);
//         routine.stop;
//     };
// });

// ~cuelist.add();

~cuelist.add( {
    var synth, routine;
    "22 tone chromatic scale".postln;
    synth = Synth(\sine, [\lag, 0.01, \amp, 0.3]);
    routine = Routine {
        var scale = Scale.new((0..21), 22, tuning: \et22, name: "et22 Chromatic");
        inf.do{
            var degree = ~mbData[9].x.linlin(0.0, 1.0, 0, 21.9).floor;
            var freq = scale.degreeToFreq(degree, 442, -1);
            synth.set(\freq, freq);
            0.05.wait;
        }
    }.play;
    ~cuelist.hook = {|cl| 
        synth.release(2);
        routine.stop;
    };
});

~cuelist.add( { ~cuelist.hook = {|cl| } });

~cuelist.add( {
    var synth, routine;
    "superpyth 7".postln;
    synth = Synth(\sine, [\lag, 0.01, \amp, 0.1]);
    routine = Routine {
        var scale = ~superpyth;
        // var scale = Scale.new([0, 4, 5, 9, 13, 17, 18], 22, tuning: \et22, name: "superpyth 7");
        inf.do{
            var degree = ~mbData[9].x.linlin(0.0, 1.0, 0, 7.9).floor;
            var freq = scale.degreeToFreq(degree, 442, -1);
            synth.set(\freq, freq);
            0.1.wait;
        }
    }.play;
    ~cuelist.hook = {|cl| 
        synth.release(3);
        routine.stop;
    };
});

~cuelist.add( { ~cuelist.hook = {|cl| } });

~cuelist.add( {
    fork{
        "into the cold".postln;
        this.executeFile(~sketchdir+/+"additiveMBTest.scd");
        s.sync;
        ~additiveMBTestFx.level = 6.dbamp;
        ~additiveMBTestFx.play;
        ~additiveMBTest[0..3].do(_.play);
    };

    ~cuelist.hook = {|cl| 
        ~additiveMBTestFx.free;
        ~additiveMBTest[0..3].do(_.free);
    };
});

~cuelist.add( { ~cuelist.hook = {|cl| } });

~cuelist.add( {
    fork{
        "arvo minus autotune".postln;
        this.executeFile(~sketchdir+/+"arvo2.scd");
        s.sync;
        ~arvo2Fx.level = 9.dbamp;
        ~arvo2Fx.play;
        ~arvo2[0..3].do(_.play);
        1.wait;
        ~arvo2Fx.fx[\autoTune].set(\mix, 0);
    };

    ~cuelist.hook = {|cl| 
        ~arvo2Fx.free;
    };
});

~cuelist.add( { ~cuelist.hook = {|cl| } });

~cuelist.add( {
    fork{
        "arvo plus autotune".postln;
        this.executeFile(~sketchdir+/+"arvo2.scd");
        s.sync;
        ~arvo2Fx.level = 12.dbamp;
        ~arvo2Fx.play;
        ~arvo2[0..3].do(_.play);
    };

    ~cuelist.hook = {|cl| 
        ~arvo2Fx.free;
    };
});

if ( ~cuelist.cueList.size > 0 ){
    "cuelist loaded".postln;
}{
    "WARNING: Cuelist not loaded".postln;
}
)

// ~cuelist.next;
