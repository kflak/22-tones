(
~chromatic  = Scale.new((0..21), 22, tuning: \et22, name: "chromatic 22");
~machine6   = Scale.new([0, 4, 6, 10, 14, 18], 22, tuning: \et22, name: "machine 6");
~porcupine7 = Scale.new([0, 3, 7, 9, 13, 16, 19], 22, tuning: \et22, name: "porcupine 7");
~porcupine8 = Scale.new([0, 3, 6, 9, 12, 15, 18, 19], 22, tuning: \et22, name: "porcupine 8");
~orwell     = Scale.new([0, 2, 5, 7, 10, 12, 15, 17, 20], 22, tuning: \et22, name: "orwell");
~superpyth  = Scale.new([0, 4, 8, 9, 13, 17, 21], 22, tuning: \et22, name: "superpyth 7");
~pajara10   = Scale.new([0, 2, 4, 7, 9, 11, 13, 15, 18, 20], 22, tuning: \et22, name: "pajara 10");
)
