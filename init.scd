( 
~root = PathName(thisProcess.nowExecutingPath).pathOnly;
~sketchdir = ~root+/+"sketches";
~numSpeakers = 2;
~numSubs = 0;
~mic = (0..7) + ~numSpeakers + ~numSubs;
~hasLaunchPad = false; 
~hasEnttec = false;
FxChain.numSpeakers = ~numSpeakers;

// useful for later debugging:
format("Initialized at %", Date.localtime).postln;

s.options.numInputBusChannels = 8;
s.options.numOutputBusChannels = ~numSpeakers + ~numSubs;
//make sure there's enough memory for the delay lines
s.waitForBoot({
    // MIDIClient.disposeClient; 
    // MIDIClient.init; 
    // MIDIClient.sources.do{|device, idx|
    //     if(device.name.contains("Launchpad MIDI 1")){
    //         MIDIIn.connect(idx, device);
    //         "% connected\n".postf(device.name);
    //         ~launchPadOut = MIDIOut.newByName("Launchpad", "Launchpad MIDI 1");
    //         ~hasLaunchPad = true;
    //         ~launchPadOut.control(176, 0, 127);
    //         ~launchPadOut.latency = 0;
    //         LPButton.midiout = ~launchPadOut;
    //         LPButton.uid = ~launchPadOut.uid;
    //         ~lp = List.new;
    //     }
    // };

    //load synthdefs
    this.executeFile(~root+/+"sdefs.scd");
    
    // get scales
    this.executeFile(~root+/+"scales.scd");

    //global settings for minibees 
    ~mb = (9..16);
    ~mbData = IdentityDictionary.new;
    ~mb.do{|id| ~mbData.put(id, MBData.new(id))};
    MBDeltaTrig.mbData = ~mbData;

    //set up master track
    ~masterGroup = Group.new();
    ~masterBus = Bus.audio(s, ~numSpeakers);
    ~master = Synth(\route, [\in, ~masterBus, \amp, 0.dbamp, \out, 0], ~masterGroup); 
    if(~numSubs > 0){
        ~sub = Synth(\mono, [\in, ~masterBus, \amp, -10.dbamp, \out, ~numSpeakers], ~masterGroup);
    };

    "mb variables set up".postln;

    this.executeFile(~root+/+"buffers.scd");

    // ~launchPadColor = (
    //     \off: 12,
    //     \redLo: 13,
    //     \redHi: 15,
    //     \amberLo: 29,
    //     \amberHi: 63,
    //     \yellow: 62,
    //     \greenLo: 28,
    //     \greenHi: 60
    // );

    // load cuelist
    // this.executeFile(~root+/+"cuelist.scd");
});
)
