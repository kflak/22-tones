# Timeline Suggestion for 22-EDO workshop

From [https://roosnaflak.com/100-sketches](https://roosnaflak.com/100-sketches)

## Chunk 1

### 025 Not Quite Ready
A rather pompous beginning. Connecting our antennae to the gods of electromagnetism. Small shifts of angle around the bellybutton.

### 028 Rooftop
Külli solo. Gremlin-sounds emanating from the depths of the earth. A human bird.

### 002 Granular Trumpet
Kenneth solo. Yet another bird, this time much lighter.

### 024 Needle Cocoon
Duet. Using each other as strings and resonators.

### 023 Hands
Ethereal hands against the sky.


## Chunk 2

### 006 Desert Thinking Space
Live recording and processing of voice through mic and movement sensors. The movements are based on constantly finding, navigating and letting go of different logics: anatomical, geometrical, experiential, imaginative.

### 009 Percussion Duet
Crispy, outwards facing, dopamine hunting.

### TODO: Mic Vector
High energy, snappy changes of direction, elegantly organized confusion, live mics. Sketch is still not made.

### 015 Making Air
Kenneth Solo. Arpeggiated madness. Needs to be reworked to 22-EDO


## Chunk 3

### TODO: Reactive FM Drone
Külli as Thor. Wide stance, sub-bass feedback fm drone, picking up the pieces and creating a new world.

### 029 Things That Don't Burn
As in the video, Kenneth picks up the Reactive FM Drone sounds.

### 032 Realities Within Realities
But now as a duet.
