(
var offset = 48;
var notes = nil ! 128;

// MIDIClient.init();
// MIDIIn.connectAll();

MIDIdef.noteOn(\andrusKeysOn, {|velocity, note|
    var sample = ~buf[\AndrusMicroPerc][note];
    sample.postln;
    notes[note+offset] = Synth(\playbuf, [\buf, sample, \amp, velocity/127, \loop, 0]);
});

MIDIdef.noteOff(\additiveKeysNoteOff, {|velocity, note|
    notes[note].set(\gate, 0);
    notes[note] = nil;
});
)

